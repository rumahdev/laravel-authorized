-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 28, 2021 at 06:22 AM
-- Server version: 8.0.21
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `authorizeddev`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `method_apps`
--

DROP TABLE IF EXISTS `method_apps`;
CREATE TABLE IF NOT EXISTS `method_apps` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `module_apps_id` int NOT NULL,
  `UserCreate` tinyint(1) NOT NULL,
  `UserRead` tinyint(1) NOT NULL,
  `UserUpdate` tinyint(1) NOT NULL,
  `UserDelete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `method_apps`
--

INSERT INTO `method_apps` (`id`, `user_id`, `module_apps_id`, `UserCreate`, `UserRead`, `UserUpdate`, `UserDelete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 0, '2021-10-28 05:18:01', '2021-10-28 05:18:01'),
(2, 1, 2, 1, 1, 1, 1, '2021-10-28 05:18:01', '2021-10-28 05:18:01'),
(3, 2, 1, 1, 1, 1, 1, '2021-10-28 05:18:01', '2021-10-28 05:18:01'),
(4, 2, 2, 1, 1, 1, 0, '2021-10-28 05:18:01', '2021-10-28 05:18:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_10_28_043400_create_module_apps_table', 1),
(5, '2021_10_28_043858_create_method_apps_table', 1),
(6, '2021_10_28_044114_create_module_user_apps_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `module_apps`
--

DROP TABLE IF EXISTS `module_apps`;
CREATE TABLE IF NOT EXISTS `module_apps` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `ModuleName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ModulePosition` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ModuleStatus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_apps`
--

INSERT INTO `module_apps` (`id`, `ModuleName`, `ModulePosition`, `ModuleStatus`, `created_at`, `updated_at`) VALUES
(1, 'Master Menu', 'Menu', 'Active', '2021-10-28 05:12:13', '2021-10-28 05:12:13'),
(2, 'Author', 'Sub Menu', 'Active', '2021-10-28 05:13:16', '2021-10-28 05:13:16'),
(3, 'Book', 'Sub Menu', 'Active', '2021-10-28 05:13:49', '2021-10-28 05:13:49');

-- --------------------------------------------------------

--
-- Table structure for table `module_user_apps`
--

DROP TABLE IF EXISTS `module_user_apps`;
CREATE TABLE IF NOT EXISTS `module_user_apps` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `module_apps_id` int NOT NULL,
  `isAccess` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_user_apps`
--

INSERT INTO `module_user_apps` (`id`, `user_id`, `module_apps_id`, `isAccess`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-10-28 05:15:00', '2021-10-28 05:15:00'),
(2, 1, 2, 0, '2021-10-28 05:15:24', '2021-10-28 05:15:24'),
(3, 1, 3, 1, '2021-10-28 05:15:50', '2021-10-28 05:15:50'),
(4, 2, 1, 1, '2021-10-28 05:16:34', '2021-10-28 05:16:34'),
(5, 2, 2, 1, '2021-10-28 05:16:49', '2021-10-28 05:16:49'),
(6, 2, 3, 1, '2021-10-28 05:17:04', '2021-10-28 05:17:04');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ibnudirsan', 'ibnudirsan@gmail.com', NULL, '$2y$10$hZ8lD9J4bDJE80YfrLkYwu8a.wcskYH8nQgrDR.hBOT2s8a90Vje2', NULL, '2021-10-27 21:50:58', '2021-10-27 21:50:58'),
(2, 'ibnu', 'ibnu@gmail.com', NULL, '$2y$10$hZ8lD9J4bDJE80YfrLkYwu8a.wcskYH8nQgrDR.hBOT2s8a90Vje2', NULL, '2021-10-27 21:51:24', '2021-10-27 21:51:24');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
