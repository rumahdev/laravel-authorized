<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@login')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/auth/author', 'AuthorController@index')->name('author.index');
Route::get('/auth/author/delete', 'AuthorController@delete')->name('author.delete');


Route::get('/auth/book', 'BookController@index')->name('book.index');
Route::get('/auth/book/delete', 'BookController@delete')->name('book.delete');

