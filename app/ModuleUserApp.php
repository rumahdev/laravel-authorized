<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleUserApp extends Model
{
    protected $fillable = [
        'user_id','module_apps_id','isAccess'
    ];
}

