<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MethodApp extends Model
{
    protected $fillable = [
        'user_id','module_apps_id','UserCreate','UserRead','UserUpdate','UserDelete'
    ];
}
