<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleApp extends Model
{
    protected $fillabel = [
        'ModuleName','ModulePosition','ModuleStatus'
    ];
}
