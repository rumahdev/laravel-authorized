<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index(){
        $this->authorize('MenuBook');
        return view('page.book.index');
    }

    public function delete(){
        $this->authorize('UserDeleteBook');

        return "OK";
    }
}
