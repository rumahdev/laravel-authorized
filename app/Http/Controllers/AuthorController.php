<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(){
        $this->authorize('MenuAuthor');
        return view('page.author.index');
    }

    public function delete(){
        $this->authorize('UserDeleteAuthor');
        return "OK";
    }
}
