<?php

namespace App\Providers\Authorized;

use App\MethodApp;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthorServiceProvicer extends ServiceProvider
{
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('UserCreateAuthor',function($user){
            $resultUserCreate = MethodApp::select('UserCreate')
                            ->where('module_apps_id',1)
                            ->where('user_id',$user->id)
                            ->first();

            return empty($resultUserCreate->UserCreate) ? false : $resultUserCreate->UserCreate;
        });

        Gate::define('UserUpdateAuthor',function($user){
            $resultUserUpdate = MethodApp::select('UserUpdate')
                            ->where('module_apps_id',1)
                            ->where('user_id',$user->id)
                            ->first();

            return empty($resultUserUpdate->UserUpdate) ? false : $resultUserUpdate->UserUpdate;
        });

        Gate::define('UserDeleteAuthor',function($user){
            $resultUserDelete = MethodApp::select('UserDelete')
                            ->where('module_apps_id',1)
                            ->where('user_id',$user->id)
                            ->first();

            return empty($resultUserDelete->UserDelete) ? false : $resultUserDelete->UserDelete;
        });
    }
}
