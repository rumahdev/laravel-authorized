<?php

namespace App\Providers\Module;

use App\ModuleUserApp;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('Menu',function($user){
            $resultUserMenu = ModuleUserApp::select('isAccess')
                            ->where('module_apps_id',1)
                            ->where('user_id',$user->id)
                            ->first();
            return empty($resultUserMenu->isAccess) ? false : $resultUserMenu->isAccess;
        });

        Gate::define('MenuAuthor',function($user){
            $resultUserMenu = ModuleUserApp::select('isAccess')
                            ->where('module_apps_id',2)
                            ->where('user_id',$user->id)
                            ->first();
            return empty($resultUserMenu->isAccess) ? false : $resultUserMenu->isAccess;
        });

        Gate::define('MenuBook',function($user){
            $resultUserMenu = ModuleUserApp::select('isAccess')
                            ->where('module_apps_id',3)
                            ->where('user_id',$user->id)
                            ->first();
            return empty($resultUserMenu->isAccess) ? false : $resultUserMenu->isAccess;
        });
    }
}
