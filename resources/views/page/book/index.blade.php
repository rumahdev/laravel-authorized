@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Page Book') }}</div>

                <div class="card-body">
                    <div class="col">
                        This Page Book
                    </div>

                    @can('UserCreateBook')
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Create') }}
                        </button>
                    </div>
                    @endcan

                    @can('UserUpdateBook')
                    <div class="col-md-6 offset-md-4 mt-2">
                        <a href="#" class="btn btn-success">
                            {{ __('Update') }}
                        </a>
                    </div>
                    @endcan

                    @can('UserDeleteBook')
                    <div class="col-md-6 offset-md-4 mt-2">
                        <a href="{{ route('book.delete') }}" class="btn btn-danger">
                            {{ __('Delete') }}
                        </a>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
