@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Page Author') }}</div>

                <div class="card-body">
                    <div class="col">
                        This Page Author
                    </div>

                    @can('UserCreateAuthor')
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Create') }}
                        </button>
                    </div>
                    @endcan

                    @can('UserUpdateAuthor')
                    <div class="col-md-6 offset-md-4 mt-2">
                        <a href="#" class="btn btn-success">
                            {{ __('Update') }}
                        </a>
                    </div>
                    @endcan

                    @can('UserDeleteAuthor')
                    <div class="col-md-6 offset-md-4 mt-2">
                        <a href="{{ route('author.delete') }}" class="btn btn-danger">
                            {{ __('Delete') }}
                        </a>
                    </div>
                    @endcan

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
